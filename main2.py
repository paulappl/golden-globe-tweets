import json
import re
import codecs
from collections import Counter

def main(jsonfile):
	awardNames =[[],"Presenter - Best Motion Picture - Drama",
					"Presenter - Best Motion Picture - Musical or Comedy",
					"Presenter - Best Director",
					"Presenter - Best Actor in a Motion Picture - Drama",
					"Presenter - Best Actor in a Motion Picture - Musical or Comedy",
					"Presenter - Best Actress in a Motion Picture - Drama",
					"Presenter - Best Actress in a Motion Picture - Musical or Comedy",
					"Presenter - Best Supporting Actor in a Motion Picture",
					"Presenter - Best Supporting Actress in a Motion Picture",
					"Presenter - Best Screenplay",
					"Presenter - Best Original Score",
					"Presenter - Best Original Song",
					"Presenter - Best Foreign Language Film",
					"Presenter - Best Animated Feature Film",
					"Presenter - Lifetime Achievement Award",
					"Presenter - Best Television Series - Drama",
					"Presenter - Best Television Series - Musical or Comedy",
					"Presenter - Best Actor in a Television Series - Drama",
					"Presenter - Best Actor in a Television Series - Musical or Comedy",
					"Presenter - Best Actress in a Television Series - Drama",
					"Presenter - Best Actress in a Television Series - Musical or Comedy",
					"Presenter - Best Miniseries or Television Film",
					"Presenter - Best Actor in a Miniseries or Television Film",
					"Presenter - Best Actress in a Miniseries or Television Film",
					"Presenter - Best Supporting Actor in a Series, Miniseries or Television Film",
					"Presenter - Best Supporting Actress in a Series, Miniseries or Television Film",
					"Best Motion Picture - Drama",
					"Best Motion Picture - Musical or Comedy",
					"Best Director",
					"Best Actor in a Motion Picture - Drama",
					"Best Actor in a Motion Picture - Musical or Comedy",
					"Best Actress in a Motion Picture - Drama",
					"Best Actress in a Motion Picture - Musical or Comedy",
					"Best Supporting Actor in a Motion Picture",
					"Best Supporting Actress in a Motion Picture",
					"Best Screenplay",
					"Best Original Score",
					"Best Original Song",
					"Best Foreign Language Film",
					"Best Animated Feature Film",
					"Lifetime Achievement Award",
					"Best Television Series - Drama",
					"Best Television Series - Musical or Comedy",
					"Best Actor in a Television Series - Drama",
					"Best Actor in a Television Series - Musical or Comedy",
					"Best Actress in a Television Series - Drama",
					"Best Actress in a Television Series - Musical or Comedy",
					"Best Miniseries or Television Film",
					"Best Actor in a Miniseries or Television Film",
					"Best Actress in a Miniseries or Television Film",
					"Best Supporting Actor in a Series, Miniseries or Television Film",
					"Best Supporting Actress in a Series, Miniseries or Television Film",
					"Unofficial: Best Dressed",
					"Hosts"]
	categoryKeywords = [[],["Presenter","Best", "Motion", "Drama"],
				["Presenter","Best", "Motion", "Musical", "Comedy"],
				["Presenter","Best Director"],
				["Presenter","Best Actor", "Motion", "Drama"],
				["Presenter","Best Actor", "Motion", "Musical", "Comedy"],
				["Presenter","Best Actress", "Motion", "Drama"],
				["Presenter","Best Actress", "Motion", "comedy", "musical"],
				["Presenter","Best supporting Actor","motion"],
				["Presenter","Best Supporting Actress", "Motion"],
				["Presenter","Best Screenplay"],
				["Presenter","Best Original Score"],
				["Presenter","Best Original Song"],
				["Presenter","Best", "Foreign", "Film"],
				["Presenter","Best Animated","film"],
				["Presenter","Lifetime Achievement"],
				["Presenter","Best", "Drama", "Series"],
				["Presenter","Best", "Comedy", "Series"],
				["Presenter","Best Actor", "Drama", "Series"],
				["Presenter","Best Actor", "Comedy", "Series"],
				["Presenter","Best Actress", "Drama", "Series"],
				["Presenter","Best Actress", "Comedy", "Series"],
				["Presenter","Best", "Mini", "Series"],
				["Presenter","Best Actor", "Mini", "Series"],
				["Presenter","Best Actress", "Mini", "Series"],
				["Presenter","Best supporting Actor", "Mini", "Series"],
				["Presenter","Best supporting Actress", "Mini", "Series"],
				["Best", "Motion", "Drama"],
				["Best", "Motion", "Musical", "Comedy"],
				["Best Director"],
				["Best Actor", "Motion", "Drama"],
				["Best Actor", "Motion", "Musical", "Comedy"],
				["Best Actress", "Motion", "Drama"],
				["Best Actress", "Motion", "comedy", "musical"],
				["Best supporting Actor","motion"],
				["Best Supporting Actress", "Motion"],
				["Best Screenplay"],
				["Best Original Score"],
				["Best Original Song"],
				["Best", "Foreign", "Film"],
				["Best Animated","film"],
				["Lifetime Achievement"],
				["Best", "Drama", "Series"],
				["Best", "Comedy", "Series"],
				["Best Actor", "Drama", "Series"],
				["Best Actor", "Comedy", "Series"],
				["Best Actress", "Drama", "Series"],
				["Best Actress", "Comedy", "Series"],
				["Best", "Mini", "Series"],
				["Best Actor", "Mini", "Series"],
				["Best Actress", "Mini", "Series"],
				["Best supporting Actor", "Mini", "Series"],
				["Best supporting Actress", "Mini", "Series"],
				["Best","Dress"],
				["host"]]
	humanCategories = [[], 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1]

	ncats = len(categoryKeywords)
	awards = [Counter() for c in range(ncats)]
	unigrams = Counter()
	with open(jsonfile,'r') as f:
		for line in f:
			tweet = json.loads(line)
			try:
				highpoints=re.compile(u'[\U00010000-\U0010ffff]')
			except re.error:
				highpoints=re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
			text = highpoints.sub(' ',tweet['text']).encode('utf8')
			#gets entities from text of tweet
			entities = cappedPhrases(text)
			words = capitalizedWords(text)
			splitEntities = []
			for ent in entities:
				elist = ent.split()
				splitEntities += elist
			words = list(set(words).difference(set(splitEntities)))
			#for w in words:
			#	unigrams[w] += 1
			#entities = awardNames(text)
			#filters entities that are too long to be real
			entities = [ent for ent in entities if not len(ent.split()) > 2]
			#filters entities that are IN ALL CAPS
			entities = [ent for ent in entities if not re.search("[A-Z]{2}",ent)]
			awardees = []
			#finds tweets where someone "won" something
			#if (" win" in text.lower() or " won" in text.lower()) and "best" in text.lower():
			#name = "best actor"
			#if name in text.lower() and "drama" in text.lower() and "series" in text.lower():
			#if "Best Motion Picture - Drama".lower() in text.lower():
			category = None
			for i in range(ncats):
				keywords = categoryKeywords[i]
				for c in keywords:
					if c.lower() in text.lower():
						awardees = entities
						entities += words
						category = i
					else:
						category = None
						break
				if awardees != [] and category:
					for a in awardees:
						skip = ["who","mini","television","the","season","miniseries","series","drama","globes","motion","tz","comedy","musical","golden","goldenglobes","best","win","won","i","and","your","academy","fuck","yeah","watch","oscar","rt"]
						skipped = False
						for s in skip:
							for p in a.split():
								if s == p.lower():
									skipped = True
						if not skipped:
							#if a not in entityContext.keys():
								#entityContext[a] = Counter()
							#for word in text.split():
							#	entityContext[a][word] += 1
							#print i
							if ' ' in a and humanCategories[i] == 1:
								if i >= 40:
									awards[i][a] += 17
								else:
									awards[i][a] += 6
							elif ' ' in a:
								awards[i][a] += 3
							else:
								awards[i][a] += 1


			#print awardees

				#print text
				#print awardees
				#print "***************"
				#print
	#print "%s: %s" % (name,awards.most_common()[0][0])
	#for a in awards.most_common()[:30]:
	#	print a[0]
	for a in range(ncats):
		notFound=True;
		if len(awards[a]) > 4 and notFound:
			notFound=False;
			winner = awards[a].most_common()[:5]
			print "%s: %s \n\tNominees:\n\t%s\n\t%s\n\t%s\n\t%s\n\n" % (awardNames[a], winner[0][0],winner[1][0],winner[2][0],winner[3][0],winner[4][0])
		elif len(awards[a]) > 3 and notFound:
			notFound=False;
			winner = awards[a].most_common()[:4]
			print "%s: %s \n\tNominees:\n\t%s\n\t%s\n\t%s\n\n" % (awardNames[a], winner[0][0],winner[1][0],winner[2][0],winner[3][0])
		elif len(awards[a]) > 2 and notFound:
			notFound=False;
			winner = awards[a].most_common()[:3]
			print "%s: %s \n\tNominees:\n\t%s\n\t%s\n\n" % (awardNames[a], winner[0][0],winner[1][0],winner[2][0])
		elif len(awards[a]) > 1 and notFound:
			notFound=False;
			winner = awards[a].most_common()[:2]
			print "%s: %s \n\tNominees:\n\t%s\n\n" % (awardNames[a], winner[0][0],winner[1][0])
		elif len(awards[a]) > 0 and notFound:
			notFound=False;
			winner = awards[a].most_common()[:1]
			print "%s: %s \n\tNo additional nominees" % (awardNames[a], winner[0][0])
		else:
			print "%s: %s\n" % (awardNames[a], "no matches found")
	#for c in entityContext.keys():
	#	print c, entityContext[c]
	 #	print
	# 	print
			
def awardNames(text):
	awards = []
	phrases = re.findall("best[ ]*[a-zA-Z]*",text.lower(),re.UNICODE)
	return phrases

def capitalizedWords(text):
	#words = re.findall("[A-Z]{1}[']*[s]*[a-z]*",text,re.UNICODE)
	words = re.findall("[A-Z]{1}[a-z]+",text,re.UNICODE)
	return words

def cappedPhrases(text):
	capped = []
	#endings like 're in we're
	phrases = re.findall("([A-Z][']*[s]*[\w-]*(?:\s+[a-z]{0,1}[A-Z][']*[s]*[\w-]*)+)",text,re.UNICODE)
	return phrases

if __name__ == "__main__":
	jsonfile = "goldenglobes.json"
	main(jsonfile)
